const inquirer = require('inquirer')
const colors = require('colors')

colors.setTheme({
  colorHover: ['cyan'],
  colorTitle: ['black', 'bgGrey'],
  colorLine: ['green', 'bold'],
  colorSubtitle: ['white', 'bold'],
  colorTask: ['white'],
  colorExit: ['white', 'bold'],
  colorNumber: ['green'],
  colorError: ['red', 'bold']
})
const options = [

  'Crear tarea',
  'Listar tareas',
  'Listar tareas completadas',
  'Listar tareas pendientes',
  'Completar tarea(s)',
  'Borrar tarea',
  'Salir'
]

const showMenu = () => {
  const line = '=========================='
  const title = '  Seleccione una opción   '
  const subtitle = '¿Qué desea hacer?'
  console.log(
    line.colorLine + '\n' +
    title.colorTitle + '\n' +
    line.colorLine + '\n')
  return inquirer
    .prompt([
      {
        type: 'list',
        name: 'taskList',
        message: subtitle,
        choices: options
      }

    ])
}
const pause = async (message) => {
  const question = [
    {
      type: 'input',
      name: 'pause',
      message: 'Presione ' + 'ENTER '.green + 'para continuar'
    }
  ]
  const { pause } = await inquirer.prompt(question)
  return pause
}

const readInput = async (message) => {
  const question = [
    {
      type: 'input',
      name: 'userInput',
      message,
      validate (value) {
        if (value.length === 0) {
          return 'Por favor ingrese un valor'
        }
        return true
      }
    }
  ]
  const { userInput } = await inquirer.prompt(question)
  return userInput
}

module.exports = { showMenu, readInput, pause }
