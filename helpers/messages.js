const colors = require('colors')
const readline = require('readline')

colors.setTheme({
  colorHover: ['cyan'],
  colorTitle: ['white'],
  colorLine: ['green', 'bold'],
  colorSubtitle: ['white', 'bold'],
  colorTask: ['white'],
  colorNumber: ['green'],
  colorError: ['red', 'bold']
})

const showMenu = () => {
  const promise = new Promise((resolve, reject) => {
    const line = '=========================='
    const title = '  Seleccione una opción'
    const subtitle = '¿Qué desea hacer?'
    const options = {

      1: 'Crear tarea',
      2: 'Listar tareas',
      3: 'Listar tareas completadas',
      4: 'Listar tareas pendientes',
      5: 'Completar tarea(s)',
      6: 'Borrar tarea',
      last: 'Salir'

    }
    const showOptions = (options) =>
      Object.entries(options)
        .map(([key, value]) => `${key === 'last' ? 0 : key}. `.colorNumber + `${value}`.colorTask)
        .join('\n')

    const readlineInterface = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })

    console.log(
      line.colorLine + '\n' +
      title.colorTitle + '\n' +
      line.colorLine + '\n' +
      subtitle.colorSubtitle + '\n' +
      showOptions(options) + '\n'
    )

    readlineInterface.question('Seleccione una opción: ', (answer) => {
      resolve(answer)
      readlineInterface.close()
    })
  })
  return promise
}

const pause = () => {
  const promise = new Promise((resolve, reject) => {
    const readlineInterface = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    })
    readlineInterface.question('Presione ' + 'ENTER '.green + 'para continuar...', (answer) => {
      resolve(answer)
      readlineInterface.close()
    })
  })
  return promise
}

module.exports = {
  showMenu,
  pause
}
