const Task = require('./Task')
class Tasks {
  constructor () {
    this.list = {}
  }

  get listArr () {
    return Object.entries(this.list).map(([, value]) => value)
  }

  createTask (desc) {
    const task = new Task(desc)
    this.list[task.id] = task
  }
}

module.exports = Tasks
