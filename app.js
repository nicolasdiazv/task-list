const { showMenu, readInput, pause } = require('./helpers/inquirer')
const Tasks = require('./models/Tasks')
console.clear()

const main = async () => {
  const tasks = new Tasks()
  let input
  let opt
  do {
    opt = await showMenu()
    switch (opt.taskList) {
      case 'Crear tarea':
        input = await readInput('Descripción: ')
        tasks.createTask(input)
        break
      case 'Listar tareas':
        console.log(tasks.listArr)
        break
      default:
        break
    }
    await pause()
  } while (opt.taskList !== 'Salir')
}

main()
